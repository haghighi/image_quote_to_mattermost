# *ImageQuote*

This script downloads a random image from [Unsplash](https://unsplash.com), retrieves a  quote from one of several quote services, writes the quote & credits into the image and posts the image into a [Mattermost](https://about.mattermost.com/) channel.

![Screenshot](https://gitlab.com/m-busche/image_quote_to_mattermost/raw/master/screencapture_small.png)

## Requirements

This script requires Python (version >= 3.5), you can download Python from https://www.python.org/downloads/.

Install Python under Debian / Ubuntu Linux:

`sudo apt-get install python3 python3-pip`

 - *ImageQuote* takes use of Mattermost API v4.
 - *ImageQuote* was tested under Windows and Linux (Ubuntu, Raspbian).

### Download *ImageQuote*

Download the [ZIP file](https://gitlab.com/m-busche/image_quote_to_mattermost/repository/master/archive.zip) or clone this project

`git clone https://gitlab.com/m-busche/image_quote_to_mattermost.git`

Afterwards, cd into the directory where you unzipped or cloned to.

### Upgrade pip and install all requirements:

> **Warning:**
*ImageQuote* relies on some dependencies. If you want to prevent breaking your 
Python setup, create a *Virtual Environment* for *ImageQuote*. See [here](http://docs.python-guide.org/en/latest/dev/virtualenvs/) for details.
>

#### Linux

`sudo pip3 install --upgrade pip`

`sudo pip3 install -r requirements.txt`

> **Note:**
You have to upgrade pip before the installation of the requirements. Else, the installation of Pillow will fail!
>

#### Windows

Start a console window with administrative rights and

`pip install --upgrade pip`

`pip install -r requirements.txt`

## Edit configuration

Copy the default configuration file `imagequote.sample.ini` to `imagequote.ini` and
edit it to suite your needs. All important settings are documented in the INI file.

> **Note:**
Don't forget to set the font in the [quote] section!
>

### Linux

`cp imagequote.sample.ini imagequote.ini`

### Windows

`copy imagequote.sample.ini imagequote.ini`

## Usage

Start the script with

### Linux

`python3 imagequote.py`

### Windows

`python imagequote.py`

## Scheduled start

You can run *ImageQuote* with your favorite scheduler (Cron under Linux, Windows Task Manager `Taskschd.msc`).

### Linux

`crontab -e`

Add one line at the end of the file to post the image at 7:00 am from Monday to Friday:

`0 7 * * 1-5 cd /directory/of/imagequote && python3 imagequote.py  >> /tmp/imagequote.log` 

(replace the paths for python3 executable and imagequote directory!)

## Help wanted

If you know a fancy quote site, let me know, perhaps we can add it as a new source!

## Credits

Special thanks to Vaelor for [Python Mattermost Driver](https://github.com/Vaelor/python-mattermost-driver), which inspired me to write this script.
